;;; -*- lexical-binding: t -*-
;; utils ------------------------------------------------------------------

(progn "basic"

       (defun pb-id (&optional a &rest _) a)

       (defun nv-re-split (re str)
         (with-temp-buffer
           (insert str)
           (goto-char (point-min))
           (re-search-forward re)
           (list (buffer-substring (point-min) (point))
                 (buffer-substring (point) (point-max)))))

       (defun eq> (x y &rest xs)
         "return the first argument if all arguments are equal"
         (when (equal x y)
           (if xs
               (apply 'eq> x xs)
             x)))

       (defun pb-kw-name (x)
         (substring (symbol-name x) 1))

       (defun pb-kw? (x)
         (equal ":" (substring (symbol-name x) 0 1)))

       (defun pb-kw->sym (x)
         (intern (pb-kw-name x)))

       (defun pb-ensure-list (x)
         (if (listp x) x (list x)))

       (defmacro pb-let (bs &rest body)
         `(let* ,(-partition 2 bs) ,@body))

       (defmacro pb-cond (&rest xs)
         `(cond ,(-partition 2 (mapcar 'pb-ensure-list xs))))

       (defmacro pb-flet (bindings &rest body)
         (if-let ((b1 (car bindings)))
             `(cl-flet (,b1)
                (pb-flet ,(cdr bindings) ,@body))
           `(progn ,@body)))

       (progn "tests"
              (pb-kw-name :sdf) ;-> "sdf"
              (macroexpand '(pb-let (a b c d) e f g))
              (macroexpand '(pb-cond a b c d (e f g) (h i j) k (l m o) (p q r) s))))

(progn "seq"

       (defun seq-apply (f xs)
         (apply f (seq-into xs 'list)))
       (defun seq-zip (a b)
         (if (car a)
             (cons (car a) (seq-zip (cdr a) (cdr b)))
           b))
       (defun seq-repeat (x n)
         (when (not (zerop n))
           (cons x (seq-repeat x (- n 1)))))
       (defun seq-iter (f n)
         (when (not (zerop n))
           (cons (call f n) (seq-iter f (- n 1)))))
       (defun seq-splat (s at)
         (cons (seq-take s at)
               (seq-drop s at))))

(progn "plists"

       (defun pl (&rest xs)
         (apply 'plput '() xs))

       (defun plget (pl k)
         (cond
          ((not k) pl)
          ((listp k) (plget (plget pl (car k)) (cdr k))) 
          ('else (plist-get pl k))))

       (defun plput-h (pl k v)
         (cond
          ((not k) v)
          ((listp k)
           (plput-h pl
                    (car k)
                    (plput-h (plget pl (car k)) (cdr k) v)))

          ('else
           (plist-put pl k v))))

       
       (defun plput (pl k v &rest kvs)
         (if kvs
             (apply' plput (plput-h pl k v) kvs)
           (plput-h pl k v)))

       (defun plupd (pl k f &rest kvs)
         (let ((nxt (plput pl k (funcall f (plget pl k)))))
           (if kvs
               (apply 'plupd nxt kvs)
             nxt)))

       (defun plmrg (pl &rest pls)
         (if pls
             (apply 'plmrg (apply 'plput pl (car pls)) (cdr pls))
           pl))

       (progn "tests"

              (pl '(:a :b) 1 :p 2)

              (plget (pl :a 1 :b 2 :c (pl :d 42))  '(:c :d))

              (plput (pl :a 1 :b 2)
                     '(:f :g) 42
                     :a 38)

              (plupd (pl :a 1 :b (pl :c 41))
                     :a '1-
                     '(:b :c) '1+)

              (plmrg (pl :a 1 :b 2)
                     (pl :a 3 :n 0)
                     (pl :p 33))))

;; nav -------------------------------------------------------------------

;; vars
(defvar opening-delimiters
  '("(" "[" "{"))
(defvar closing-delimiters
  '(")" "]" "}"))
(defvar space-chars
  '(" " "\t" "\n" "\r" "\f" "\v"))
(defvar delimiters
  (apply 'append opening-delimiters closing-delimiters))
(defun space-char? (c)
  (member c space-chars))

;; impl
(defun sp-sel->nv-sel (s)
  (list :beg (plget s :beg) :end (plget s :end)))

;; constructors
(defun nv-sel (x y)
  (pl :beg (min x y) :end (max x y)))
(idefun nv-sel-at (&optional c)
        (if c
            (save-excursion (goto-char c) (sp-sel->nv-sel (sp-get-thing)))
          (sp-sel->nv-sel (sp-get-thing))))
(defun nv-point (p)
  (nv-sel p p))

;; inspection
(defun nv-beg (s)
  (plget s :beg))
(defun nv-end (s)
  (plget s :end))
(defun nv-bbeg (s)
  (1- (nv-beg s)))
(defun nv-aend (s)
  (1+ (nv-end s)))
(defun nv-str (s)
  (buffer-substring-no-properties (nv-beg s) (nv-end s)))
(defun nv-size (s)
  (- (nv-end s) (nv-beg s)))
(defun nv-size= (s n)
  (equal n (nv-size s)))
(defun nv-empty? (s)
  (equal (nv-beg s) (nv-end s)))
(defun nv-unit? (s)
  (nv-size= s 1))
(defun nv-beg-char (s)
  (and (not (nv-empty? s)) (substring (nv-str s) 0 1)))
(defun nv-end-char (s)
  (and (not (nv-empty? s)) (substring (nv-str s) -1 0)))

;; transformations
(defun nv-beg! (s v)
  (nv-sel v (nv-end s)))
(defun nv-end! (s v)
  (nv-sel (nv-beg s) v))
(defun nv-beg+ (s n)
  (nv-sel (v (+ n (nv-beg s))) (nv-end s)))
(defun nv-end+ (s n)
  (nv-sel (nv-beg s) (v (+ n (nv-end s)))))

;; interactive
(comment "refactor wip"
         (defmacro nv-silently-at (s &rest body)
           `(save-excursion
              (goto-char (nv-beg ,s))
              ,@body
              (nv-sel-at)))
         (idefun nv-first? (s)
           (eq> s (nv-silently-at
                   s (when (sp-get-enclosing-sexp)
                       (sp-backward-up-sexp)
                       (sp-down-sexp))))))

;; predicates
(defun nv-expr? (s)
  (and (member (nv-beg-char s) delimiters) s))
(defun nv-empty-expr? (s)
  (and (nv-expr? s) (equal (nv-beg s) (- (nv-end s) 2)) s))
(defun nv-word? (s)
  (and (not (member (nv-beg-char s) delimiters))
       s))
(defun nv-top? (s)
  (and (not (sp-get-enclosing-sexp)) s))
(defun nv-bottom? (s)
  (nv-word? s))
(idefun nv-first? (s)
  (save-excursion
    (goto-char (nv-beg s))
    (when (sp-get-enclosing-sexp)
      (sp-backward-up-sexp)
      (sp-down-sexp)
      (and (equal s (nv-sel-at)) s))))
(idefun nv-last? (s)
  (save-excursion
    (goto-char (nv-beg s))
    (when (sp-get-enclosing-sexp)
      (sp-backward-up-sexp)
      (sp-forward-sexp)
      (sp-backward-down-sexp)
      (sp-backward-sexp)
      (and (equal s (nv-sel-at)) s))))

;; native moves
(idefun nv-prev (s)
        (and (not (nv-first? s))
             (save-excursion
               (goto-char (nv-beg s))
               (sp-backward-sexp)
               (nv-sel-at))))
(idefun nv-next (s)
        (and (not (nv-last? s))
             (save-excursion
               (goto-char (nv-beg s))
               (sp-forward-sexp 2)
               (sp-backward-sexp)
               (nv-sel-at))))
(idefun nv-out (s)
        (and (not (nv-top? s))
             (save-excursion
               (goto-char (nv-beg s))
               (sp-backward-up-sexp)
               (nv-sel-at))))
(idefun nv-in (s)
  (and (not (or (nv-bottom? s)
                (nv-empty-expr? s)))
       (save-excursion
         (goto-char (nv-beg s))
         (sp-down-sexp)
         (nv-sel-at))))

;; moves compositor
(progn "preds"
       (defun list! (x)
         (seq-into x 'list))
       (defun seq? (x)
         (or (listp x) (vectorp x)))
       (defun atom? (x)
         (not (seq? x))))

(progn "destructuring function WIP"

       (defun pb-destructure (a b)
         (cond
          ((atom? a) (list a b))
          ((seq? a)
           (when-let ((a (seq-into a 'list))
                      (b (seq-zip (seq-into b 'list) (seq-repeat nil (length a))))
                      (ca (car a)))
             (cons (pb-destructure ca (car b))
                   (pb-destructure (cdr a) (cdr b)))))))
       (progn "destr tries"
              (pb-destructure () '(1 2 3))
              (pb-destructure 'a '(1 2 3))
              (pb-destructure '(a b c) '(1 2 3))
              (pb-destructure '(a b c) [1 2 3])
              (pb-destructure '(a b c) nil)
              (pb-destructure '(a b c) '(1 2))
              (pb-destructure '(a b c) '(1 2 3))
              )
       (defun gensyms (n)
         (seq-iter 'gensym n))
       (defmacro f (args &rest body)
         (let ((syms (gensyms (length args))))
           `(fn ,syms
                (let ,(pb-destructure args syms)
                  ,@body))))

       (comment
        (macroexpand-1 '(f ((a b) c d) (list a b c d)))

        (call (f ((a b) c d) (list a b c d))
          '((1 2) 3 4))))

(progn "nv-mv new"

       ;; this var will hold user defined moves
       (setq nv-ops ())

       (defmacro nv-op (args expr)
         "syntax for creating nv-op
          ex: (nv-op (selection arg1 arg2)
                     ...compute_new_selection)"
         (let ((len (- (length args) 1)))
           `(fn (xs cont)
                (call cont
                  (fn (s) (apply (fn ,args ,expr) (cons s (seq-take xs ,len))))
                  (seq-drop xs ,len)))))

       (comment
        (macroexpand-1
         '(nv-op (s b c) (pouet s b c))))

       (defmacro nv-op+ (k args expr)
         "add a new move to nv-op (using 'nv-op syntax)"
         `(setq nv-ops
                (plput nv-ops ,k (nv-op ,args ,expr))))

       (progn "ops"

              (nv-op+ :skip1 (s _) (progn (print "skip1") s))

              (nv-op+ :when
                      (s p e)
                      (when (call (nv-mv p) s)
                        (call (nv-mv e) s)))

              (nv-op+ :if
                      (s p t f)
                      (if (call (nv-mv p) s)
                          (call (nv-mv t) s)
                        (call (nv-mv f) s)))

              (nv-op+ :opt
                      (s e)
                      (or (call (nv-mv e) s) s))

              (nv-op+ :or
                      (s a b)
                      (or (call (nv-mv a) s)
                          (call (nv-mv b) s)))

              (nv-op+ :all
                      (s xs)
                      (call (seq-apply 'nv-mv xs) s))

              (nv-op+ :any
                      (s mvs)
                      (when-let ((mvs (seq-into mvs 'list)))
                        (or (call (nv-mv (car mvs)) s)
                            (and (cdr mvs) (call (nv-mv :any (cdr mvs)) s)))))

              (nv-op+ :deep
                      (s mv)
                      (if-let ((nxt (call (nv-mv mv) s)))
                          (call (nv-mv :deep mv) nxt)
                        s))

              (nv-op+ :pr
                      (s id)
                      (progn 
                        (princ (concat (symbol-name id) ": " (nv-str s) "\n"))
                        s)))

       (defun nv-mv (&rest xs)
         "move compositor:
          return a move :: sel -> sel
          TODO examples (for now see further defined moves)"
         (cl-flet

             ((cont (f xss)
                    (fn (s)
                        (when s
                          (call (seq-apply 'nv-mv xss)
                            (call f s))))))

           (cond
            ;; if no args return identity funtion
            ((not xs) (fn (s) s))
            ;; if first arg is a seq apply and continue
            ((seq? (car xs))
             (cont (cont 'pb-id (car xs))
                   (cdr xs)))
            ;; first arg is a defined operator (see nv-op)
            ((pb-kw? (car xs))
             (if-let ((impl (plget nv-ops (car xs))))
                 (call impl (cdr xs) (function cont))
               (error "Unknown expression %S" (car xs))))
            ;; first arg is a move
            (:else
             (cont (car xs) (cdr xs)))))))

(comment "nv-mv old"
 (defun nv-mv (&rest xs)

   (cl-flet ((cont (f xss)
              (fn (s)
                  (when s
                    (call (seq-apply 'nv-mv xss)
                      (call f s))))))

     (cond

      ((not xs) (fn (s) s))

      ((vectorp (car xs))
       (cont (cont 'pb-id (car xs))
             (cdr xs)))
      ;; those two add to be one 'seqp
      ((listp (car xs))
       (cont (cont 'pb-id (car xs))
             (cdr xs)))

      ((pb-kw? (car xs))
       (pcase (car xs)

         (:when
          (cont
           (fn (s)
               (when (call (nv-mv (cadr xs)) s)
                 (call (nv-mv (caddr xs)) s)))
           (cdddr xs)))

         (:if
          (cont
           (fn (s)
               (if (call (nv-mv (cadr xs)) s)
                   (call (nv-mv (caddr xs)) s)
                 (call (nv-mv (cadddr xs)) s)))
           (cddddr xs)))

         (:opt
          (cont
           (fn (s)(or (call (nv-mv (cadr xs)) s) s))
           (cddr xs)))

         (:or
          (cont
           (fn (s)(or (call (nv-mv (cadr xs)) s)
                      (call (nv-mv (caddr xs)) s)))
           (cdddr xs)))

         (:all
          (cont
           (fn (s) (call (seq-apply 'nv-mv (cadr xs)) s))
           (cddr xs)))

         (:any
          (cont
           (fn (s)
               (when-let ((mvs (seq-into (cadr xs) 'list)))
                 (or (call (nv-mv (car mvs)) s)
                     (and (cdr mvs) (call (nv-mv :any (cdr mvs)) s)))))
           (cddr xs)))

         (:deep
          (cont
           (fn (s)
               (if-let ((mv (cadr xs))
                        (nxt (call (nv-mv mv) s)))
                   (call (nv-mv :deep mv) nxt)
                 s))
           (cddr xs)))

         (:pr
          (cont
           (fn (s)
               (princ (concat (symbol-name (cadr xs)) ": " (nv-str s) "\n"))
               s)
           (cddr xs)))

         (_ (error "Unknown expression %S" (car xs)))))

      (:else
       (cont (car xs) (cdr xs)))))))

(comment "move tests"

         (call (nv-mv 'nv-next) (nv-sel-at))
         (call (nv-mv 'nv-next 'nv-next) (nv-sel-at))
         (call (nv-mv '(nv-next nv-prev)) (nv-sel-at))
         (call (nv-mv [nv-next nv-prev]) (nv-sel-at))
         (call (nv-mv :or 'nv-next 'nv-prev) (nv-sel-at))
         (call (nv-mv :all [nv-next nv-prev]) (nv-sel-at))
         (call (nv-mv :any [nv-next [:or nv-prev nv-out]]) (nv-sel-at))
         (call (nv-mv :deep 'nv-out) (nv-sel-at)))

;; conveniance macro 
(defmacro nv-defmv (name mvs)
  `(defalias ',name
     (nv-mv ,(seq-into mvs 'vector))))

;; deep moves
(nv-defmv nv-deep-next [:deep nv-next])
(nv-defmv nv-deep-prev [:deep nv-prev])
(nv-defmv nv-deep-out [:deep nv-out])
(nv-defmv nv-deep-in [:deep nv-in])

;; nav moves
(nv-defmv nv-fw
          [:any [nv-next
                 [nv-out nv-next ;:opt nv-in
                         ]
                 [nv-out nv-fw]]])
(nv-defmv nv-bw
          [:or nv-prev
               [nv-out nv-bw ;:opt [nv-in nv-deep-next]
                       ]])
(nv-defmv nv-fwin
          [:any [nv-in nv-fw]])
(nv-defmv nv-bwin
          [:any [[nv-first? nv-out]
                 [nv-prev :opt [:deep [nv-in nv-deep-next]]]
                 nv-prev]]
          ;; [:any [[nv-bw nv-in nv-deep-next] [nv-bw nv-in] nv-bw]]
          )
(nv-defmv nv-ffw
          [:or nv-deep-next
               [nv-out nv-fw :opt nv-in]])
(nv-defmv nv-fbw
          [:or nv-deep-prev
               [nv-out nv-bw :opt [nv-in nv-deep-next]]])
(nv-defmv nv-fwout
          [:if nv-top?
               nv-fw
               [:or [nv-out nv-fw] [nv-out nv-fwout]]])

;; state
(defvar nv-state
      (pl :current (pl :beg 0 :end 0)
          :reverse nil))

;; mark
(idefun nv-mark-on! ()
  (setq deactivate-mark nil))
(idefun nv-mark-off! ()
  (setq deactivate-mark t))
(defmacro with-mark-off (&rest body)
  `(progn
    (nv-mark-off!)
    ,@body
    (nv-mark-on!)))

;; clean
(idefun nv-clean-fw! ()
  (let ((c (nv-current)))
    (save-excursion
      (goto-char (nv-end c))
      (when (space-char? (current-char))
        (hungry-delete-forward 1)))))
(idefun nv-clean-bw! ()
  (let ((c (nv-current)))
    (save-excursion
      (goto-char (nv-bbeg c))
      (when (space-char? (current-char))
        (hungry-delete-backward 1)))))
(idefun nv-clean-around! ()
  (let ((c (nv-current)))
    (with-mark-off
     (when (not (nv-first? c)) (nv-clean-bw!))
     (when (not (nv-last? c)) (nv-clean-fw!)))))

;; side effects
(idefun nv-mark! (s)
        ;(dbg 'nv-mark s)
        (goto-char (nv-beg s))
        (set-mark (nv-end s)))
(idefun nv-set! (v)
  (setq nv-state v))
(idefun nv-current ()
  (plget nv-state :current))
(idefun nv-current! (&optional s)
        (let ((s (or s (nv-sel-at))))
          (nv-set! (plput nv-state :current s))
          s))
(idefun nv-goto-beg! ()
  (goto-char (nv-beg (nv-current))))
(idefun nv-goto-end! ()
  (goto-char (nv-end (nv-current))))
(idefun nv-upd-current! (f)
        (nv-current! (funcall f (nv-current))))
(idefun nv-reverse! (&optional b)
  (nv-set!
   (cond
    ((or (not b) (= 0 b)) (plupd nv-state :reverse 'not))
    ((> b 0) (plput nv-state :reverse t))
    ((< b 0) (plput nv-state :reverse nil)))))
(idefun nv-mark-current! (&optional s) 
        (nv-mark! (if s (nv-current! s) (nv-current)))
        (when (plget nv-state :reverse)
          (exchange-point-and-mark)))
(idefun nv-mark-at-point! ()
  (nv-mark-current! (nv-sel-at)))
(idefun nv-splice-current! ()
        (let ((c (nv-current)))
          (nv-in c)
          (save-excursion
            (goto-char (nv-beg (nv-current)))
            (paredit-splice-sexp))
          (nv-current!)
          (nv-mark-on!)
          (nv-mark-current!)))
(idefun nv-wrap-current!-old ()
        (goto-char (nv-beg (nv-current)))
        (paredit-wrap-round)
        (nv-current!)
        (nv-mark-on!)
        (nv-mark-current!))
(idefun nv-wrap-current! (&optional op cl)
  (let ((e (nv-str (nv-current))))
    (call-interactively 'delete-region)
    (insert (concat (or op "(") e (or cl ")")))
    (backward-char 2)
    (nv-mark-on!)
    (nv-current!)
    (nv-mark-current!)))
(idefun nv-copy-current! ()
  (kill-new (nv-str (nv-current))))
(idefun nv-kill-current! ()
  ;; TODO when point at end erase too much
  (let ((c (nv-current)))
    (goto-char (nv-beg c))
    (nv-mark-off!)
    (sp-kill-sexp)
    (when (space-char? (current-char)) (hungry-delete-forward 1))
    (when (space-char? (save-excursion (backward-char 1) (current-char))) (hungry-delete-backward 1))
    (nv-mark-on!)
    (backward-char 1)
    (nv-mark-at-point!)))

(idefun nv-kill-current! ()
  ;; TODO when point at end erase too much
  (let ((c (nv-current)))
    (goto-char (nv-beg c))
    (nv-mark-off!)
    (sp-kill-sexp)
    (nv-current! (nv-prev c))
    (nv-clean-around!)
    (nv-mark-on!)
    (nv-mark-at-point!)))

(idefun nv-indent-current! ()
        (let* ((c (nv-current!))
               (c (if (nv-word? c) (nv-out c) c)))
          (indent-region (nv-beg c) (nv-end c))))
(idefun nv-paste! ()
        (call-interactively 'delete-region)
        (insert (current-kill 0))
        (nv-mark-on!)
        (backward-char 1)
        (nv-current!)
        (nv-reverse! -1)
        (nv-mark-current!))
(defun nv-fold-cycle! (arg)
  (interactive "p")
  (let* ((c (nv-current))
         (fold-state? (plget c :fold-state))
         (fold-state (or fold-state? (if (hs-already-hidden-p) :folded :unfolded))))
    (cond
     ((equal fold-state :unfolded)
      (hs-hide-block)
      (nv-mark-current! (plput (nv-sel-at) :fold-state :folded)))
     ((equal fold-state :folded)
      (hs-hide-level arg)
      (nv-mark-current! (plput (nv-sel-at) :fold-state :unfolding)))
     ((equal fold-state :unfolding)
      (hs-show-block)
      (nv-mark-current! (plput (nv-sel-at) :fold-state :unfolded))))))

(idefun nv-return-k ()
  (setq hungry-delete-chars-to-skip " \t\f\v")
  (goto-char (nv-beg (nv-current)))
  (nv-mark-off!)
  ;; TODO you like pain little pervert
  (hungry-delete-backward 1)
  (insert "\n")
  (indent-for-tab-command)
  (nv-current!) (nv-mark-current!)
  ;; TODO seriously? make a function!
  (nv-mark-on!))


(idefun nv-backspace-k ()
  (setq hungry-delete-chars-to-skip " \n\t\f\v")
  (nv-reverse! -1)
  (goto-char (nv-beg (nv-current)))
  (nv-mark-off!)
  ;; TODO abstract this pattern
  (when (save-excursion
          (backward-char 1)
          (space-char? (current-char)))
    (hungry-delete-backward 1)
    (insert " "))
  (nv-rdeactivate-mark)
  ;; TODO i've already seen this somewhere...
  (nv-current!) (nv-mark-current!)
  )

(idefun nv-new-fw ()
  ;; TODO nv-point-at-end would be more explicit
  (nv-reverse! 1)
  (nv-mark-current!)
  (insert " _")
  (nv-upd-current! 'nv-next)
  (nv-mark-current!)
  (nv-reactivativate-mark))
(idefun nv-new-bw ()
  ;; TODO nv-point-at-beg would be more explicit
  (nv-reverse! -1)
  (nv-mark-current!)
  (insert "_ ")
  (backward-char 2)
  (nv-current!) (nv-mark-current!)
  (nv-reactivativate-mark))
(idefun nv-space-k ()
  (call-interactively
   (if (plget nv-state :reverse)
       'nv-new-fw
     'nv-new-bw)))

(idefun nv-insert-fw ()
  "exit nav mode a place cursor after current exp"
  (nv-reverse! 1)
  (nv-mark-current!)
  (nav-mode -1)
  (insert " "))
(idefun nv-insert-bw ()
  "exit nav mode a place cursor before current exp"
  (nv-reverse! -1)
  (nv-mark-current!)
  (nav-mode -1)
  (insert " ")
  (backward-char))

(progn "xp"
       (idefun nv-parse-region ()
         (print (parse-partial-sexp (point) (mark)))))

(progn "nav-mode"

  (defvar nav-mode-map (make-sparse-keymap))

  (defun init-escape-keys (x &rest xs)
    (defmks nav-mode-map x (ifn ()(nav-mode -1) (evil-set-cursor-color "#fafafa") (nv-mark-off!)))
    (when xs (apply 'init-escape-keys xs)))
  (init-escape-keys
   "<escape>" 
   "<down>" 
   "<up>"
   )

  (defmks nav-mode-map

    ;; dev
    "n" (ifn () (print "nv-next") (nv-upd-current! 'nv-next) (nv-mark-current!))
    "b" (ifn () (print "nv-next") (nv-upd-current! 'nv-prev) (nv-mark-current!))
    "K" (ifn () (nv-clean-around!) (nv-mark-current!))
    "L" (ifn () (nv-clean-fw!) (nv-mark-current!))
    "J" (ifn () (nv-clean-bw!) (nv-mark-current!))

    "i" (ifn () (nv-upd-current! 'nv-bw)  (nv-mark-current!))
    "o" (ifn () (nv-upd-current! 'nv-fw)  (nv-mark-current!))
    "u" (ifn () (nv-upd-current! 'nv-deep-prev)  (nv-mark-current!)) 
    "p" (ifn () (nv-upd-current! 'nv-deep-next)  (nv-mark-current!))
    "j" (ifn () (nv-upd-current! 'nv-out)  (nv-mark-current!))
    "m" (ifn () (nv-upd-current! 'nv-fwout)  (nv-mark-current!)) 
    "k" (ifn () (nv-upd-current! 'nv-bwin)  (nv-mark-current!))
    "l" (ifn () (nv-upd-current! 'nv-fwin)  (nv-mark-current!))

    "h" (ifn () (nv-reverse!) (nv-mark-current!))
    "," (ifn () (dbg 's (nv-str (nv-sel-at))))

    "SPC" 'nv-space-k
    "<return>" 'nv-return-k
    "<backspace>" 'nv-backspace-k

    "(" 'nv-wrap-current!
    "s-(" (ifn () (nv-wrap-current! "[" "]"))
    "M-(" (ifn () (nv-wrap-current! "{" "}"))
    "c" 'nv-copy-current!
    "w" 'nv-kill-current!
    "x" 'nv-kill-current!
    "v" 'nv-paste!
    "s" 'nv-splice-current!
    "q" 'nv-wrap-current!
    "<tab>" 'nv-fold-cycle!

    "s-o" 'nv-new-fw
    "s-i" 'nv-new-bw

    "<left>" (ifn () (nv-reverse! -1) (nav-mode -1) (nv-mark-off!))
    "<right>"(ifn () (nv-reverse! 1) (nav-mode -1) (nv-mark-off!)) 

    )

  (comment
   (+ 1 rte 2 (aze baz [1 2] )
      (aze baz [1 2] )
      (aze baz [1 2] )))

  (define-minor-mode nav-mode
    :init-value nil
    :lighter " Nav"
    :keymap nav-mode-map
    :group 'nav)

  (defks "s-j"
    (ifn ()
         (nav-mode 1)
         (set-cursor-color "pink") 
         (nv-current!)
         (nv-mark-current!))))

(comment
 (abc 1i
      (+ 12
         (+ 12
            ert
            (iop df 3)
            (z df 42)
            o)
         (iop df 3) 
         (z df 42)
         o)
      (+ 12
         (+ 12
            ert
            (iop df 3)
            (z df 42)
            o)
         (iop df 3)
         (z df 42)
         o)
      (p oi l)))

(comment
 (defun nv-parse-sexp ()
   (cond
    ((symbol-at-point)
     (let ((v (pl :type :symbol :at (point))))
       (forward-symbol)
       v))
    ((sexp-at-point)
     (let ((v (pl :type :list :at (point) :form (sexp-at-point)))
           (children (sexp-at-point))))
     (pl :type :list
         :at (point)
         :children
         (progn (forward-char)
                (mapcar (fn (_) (nv-parse-sexp)) children)))
     )
    )))
